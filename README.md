# How to Run

Run the following:
> npm install

> SET DEBUG=leasetrack-backend:* & npm start

Then load `http://localhost:3000/` in your browser to access the app.